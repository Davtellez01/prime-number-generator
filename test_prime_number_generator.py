import unittest
from prime_number_generator import is_prime, prime_generator

class TestPrimeNumGenerator(unittest.TestCase):
    def test_is_prime(self):
        self.assertFalse(is_prime(-1))
        self.assertFalse(is_prime(0))
        self.assertFalse(is_prime(1))
        self.assertTrue(is_prime(2))
        self.assertFalse(is_prime(10))
        self.assertTrue(is_prime(7901))
        self.assertTrue(is_prime(7907))
        self.assertTrue(is_prime(7919))
        self.assertFalse(is_prime(7920))

    def test_prime_generator(self):
        self.assertEqual(prime_generator(1,10), [2,3,5,7])
        self.assertEqual(prime_generator(10,1), [2,3,5,7])
        self.assertEqual(prime_generator(7900,7920), [7901, 7907, 7919])
        self.assertEqual(prime_generator(1,101), [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101])
        self.assertEqual(prime_generator(101,1), [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101])
        self.assertEqual(prime_generator(0,0), [])
        self.assertEqual(prime_generator(-1,1), [])
        self.assertEqual(prime_generator(2, 2), [2])

if __name__ == "__main__":
    unittest.main()
