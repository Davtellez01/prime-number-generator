def prime_generator(start:int, end:int):
    """
    Generates an array of all valid prime numbers found in a given range.

    :param start: The first endpoint of the given range (Inclusive).
    :param end: The last endpoint of the given range (Inclusive).
    :return: Array containing all prime numbers found in the given range.
    """

    prime_array = []

    # If there is an inverse range, swap the endpoints
    if start > end:
        start, end = end, start

    for num in range(start, end + 1):
        if is_prime(num):
            prime_array.append(num)

    return prime_array

def is_prime(value:int):
    """
    Determines whether a given integer is prime.

    :param value: A valid integer to check if prime.
    :return: True if the integer is a prime number, False if not.
    """

    # Any number that is less than 2 cannot be prime
    if value < 2:
        return False

    # Check for factors up to the square root of value
    for i in range(2, int(value**0.5) + 1):
        if value % i == 0:
            return False

    return True

def getIntInput(inputMessage:str):
    """
    Generates a prompt to get a value from user and verifies that the input is an integer.
    The prompt will appear again if an invalid input is detected.

    :param inputMessage: The prompt that will appear on the user's console to indicate what value they should input
    :return: Valid integer
    """

    while True:
        try:
            value = int(input(inputMessage))
        except ValueError:
            print("Invalid input. Please try again.")
        else:
            return value

def main():
    start = getIntInput("Enter the first integer of the range: ")
    end = getIntInput("Enter the last integer of the range: ")

    print(prime_generator(start,end))

if __name__ == "__main__":
    main()
